
package pe.edu.unmsm.sistemas.ed.modelo;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
    
    public static final String BASE = "cajas";
    public static final String URL = "jdbc:mysql://localhost:3306/"+BASE; 
    public static final String USERNAME = "root";
    public static final String PASSWORD = "1234";
    private Connection con = null;

    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = (Connection) DriverManager.getConnection(this.URL, this.USERNAME, this.PASSWORD);
        } catch (Exception e) {
            System.out.println(e);
        }
        return con;
    }
}
