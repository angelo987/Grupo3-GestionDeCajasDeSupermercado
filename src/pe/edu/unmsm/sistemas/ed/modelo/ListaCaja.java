/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.sistemas.ed.modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Familiar
 */
public class ListaCaja {
    
    NodoCaja cabecera=null;
    
    
    /*
    public static final String URL="jdbc:mysql://localhost:3306/jefe";
    public static final String USERNAME="root";
    public static final String PASSWORD="aNGeLo987";
    
    public static Connection getConnection(){
        Connection con=null;
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con=(Connection)DriverManager.getConnection(URL,USERNAME,PASSWORD);
            JOptionPane.showMessageDialog(null,"Conexion Exitosa");
        }
        catch(Exception e){
            e.getMessage();
        }
        return con;
    }
    */
    
    public ListaCaja(){
        /*
        for(int i=1;i<3;i++){
            
            Connection con=null;
        
            try {
                con = getConnection();
                ps = con.prepareStatement("SELECT * FROM usuarios_caja WHERE id = ? ");
                ps.setInt(1,i );

                rs = ps.executeQuery();

                if (rs.next()) {
                    
                    agregarCaja(i,new Usuario(rs.getString("usuario"),rs.getString("password"),rs.getString("nombre")));
                    JOptionPane.showMessageDialog(null, "Se creo la caja correctamente");
                } else {
                    JOptionPane.showMessageDialog(null, "No se pudo crear la caja");
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
       */
    }
    
    //Agregamos la caja en base al numero de caja que tiene

    
    public void agregarCaja(int num,Usuario u){
        boolean a=true;
        int i=1; //se usara para saber si es la priemra comparacion o no
        
        NodoCaja nuevo= new NodoCaja(num,u);
        NodoCaja aux=cabecera,aux2=cabecera;
        
        //agregando la caja
        if(cabecera==null){
            cabecera=nuevo; //si al lista esta vacio se agrega al inicio la caja
        }
        else{
            while(a){ //entrar en un bucle mientras 'a' sea verdadero
                if (nuevo.c.getnCaja() < aux.c.getnCaja()) { //comparamos el numero de la nueva caja  con el numero de caja del nodo
                    nuevo = aux;        
                    if(i==1){
                        cabecera=nuevo; //si es la primera pasa, la caja se agrega al inicio
                    }
                    else{
                      aux2 = nuevo;  //si no es la priemra caja se usara para agregar entre nodos
                    }
                    a = false; //si la caja se agrega correctamente 'a' cambia a falso y termina el bucle
                } else if (nuevo.c.getnCaja() == aux.c.getnCaja()) {
                    a = false; //si la caja ya existe no se cambia nada, 'a' se vuelve falso y sale del bucle
                } else if (nuevo.c.getnCaja()> aux.c.getnCaja()) { //comparamos el numero de la nueva caja  con el numero de caja del nodo
                    if (aux.sgte == null) { //si lo que sigue al nodo es vacio, significa que estamos en el ultimo nodo de la lista
                        aux.sgte = nuevo;  //siendo ese el caso agregamos la caja al final
                        a = false;//si la caja se agrega correctamente 'a' cambia a falso y termina el bucle
                    } else { //si no sigue un vacio, el nodo pasa al siguiente nodo para volver a comparar
                        aux2 = aux; // se guarda el nodo actual en aux2
                        aux = aux.sgte; //el nodo actual aux pasa al siguietne nodo
                        if(aux==null){ //si se rrecorrio toda la lista y la caja no se ah agregado llegando a null
                            a=false;  //se llego al ultimo nodo sin agregar la caja, 'a' cambia a falso y termina el bucle
                        }
                    }

                }
                i++;
            }
            
        }
    }
    
    public  boolean eliminarCaja(int num){
        int i=1;
        boolean a=true;
        NodoCaja aux=cabecera,aux2=null;
        
        while(aux!=null && a==true){ //el bucle se realiza mientras el nodo no este vacio o 'a' sea verdadero
            if(aux.c.getnCaja()==num){ //se buscara la caja mediante su numero
                if(i==1){           //si es el primer recorrido se debe eliminar al inicio
                   cabecera=aux.sgte;    
                }
                else{               //de lo contrario se usa dos nodos auxiliares para eliminar entre nodos
                    aux2.sgte=aux.sgte;
                }
                a=false;            //a se vuelve falso para indicar que la accion esta echa
            }
            else{                   //si el numero de caja no coincide los nodos auxiliares avanzaran al siguiente nodo
                aux2=aux;           //hasta que se encuentre la caja indicada o se llegue al ultimo nodo
                aux=aux.sgte;
            }
            i++; //acumulador nos indica que ya salio del primer recorrido del bucle
        }
        
        return a;
    }
    
    public String mostrarCaja(){  //mostraremos las cajas dentro de la lista
        String result="\n";
        NodoCaja aux=cabecera;
        
        while(aux!=null){
            result=result+"Caja "+aux.c.getnCaja()+"\n"; //se mostrara las cajas con su numero
            aux=aux.sgte;
        }
        return result;
    
    }
    
    public Caja elegirCaja(int num){ //eligiremos una caja mediante su numero ingresado por el usuario
        NodoCaja aux=cabecera;
        boolean a=true;
        while(aux!=null && a==true){ //el bucle se realizara hasta que se llegue al ultimo nodo o 'a' cambie a falso
            if(aux.c.getnCaja()==num){ //si el numero de caja coincide al que buscamos 'a' se vuelve falso
                a=false;            //para indicar que la accion esta echa
            }
            else{
                aux=aux.sgte;       //de lo contrario el nodo pasa al siguiente nodo hasta culminar el bucle
            }
        }
        
        return aux.c; //se retorna la caja
    }
    
    public boolean buscarNumCaja(int num){
        NodoCaja aux=cabecera;
        boolean a=false;
        while(aux!=null){
            if(aux.c.getnCaja()==num){
                a=true;
            }
        }
        
        return a;
    }
}
