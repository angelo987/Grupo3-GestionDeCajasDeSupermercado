/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.sistemas.ed.modelo;

/**
 *
 * @author Familiar
 */
public class NodoCaja {
    Caja c;
    NodoCaja sgte=null;

    public NodoCaja(int numero,Usuario u){
        c=new Caja(numero,u);
    }
        
    public Caja getC() {
        return c;
    }

    public void setC(Caja c) {
        this.c = c;
    }

    public NodoCaja getSgte() {
        return sgte;
    }

    public void setSgte(NodoCaja sgte) {
        this.sgte = sgte;
    }
    
    
}
