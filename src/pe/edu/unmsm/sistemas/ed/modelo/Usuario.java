/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.sistemas.ed.modelo;

/**
 *
 * @author cristian
 */
public class Usuario {
    
    private int id;
    private int nCaja;
    private String usser;
    private String password;
    private String nombre;
    
    public Usuario(){
        
    }
    
    public Usuario(int id,int nCaja,String usser,String password,String nombre){
        this.id=id; 
        this.nCaja=nCaja;
        this.usser=usser;
        this.password=password;
        this.nombre=nombre;   
    }    

    public int getnCaja() {
        return nCaja;
    }

    public void setnCaja(int nCaja) {
        this.nCaja = nCaja;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsser() {
        return usser;
    }

    public void setUsser(String usser) {
        this.usser = usser;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }   
}
