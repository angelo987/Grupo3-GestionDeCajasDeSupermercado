/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.sistemas.ed.modelo;

import java.util.Comparator;
import java.util.Iterator;

/**
 *
 * @author LABORATORIO 6
 */
public class ListaDoble<T> {

    NodoDoble<T> cabecera;

    private class IteradorDescendente implements Iterator<T> {

        NodoDoble<T> aux;

        public IteradorDescendente() {
            aux = cabecera;
        }

        @Override
        public boolean hasNext() {
            return aux != null;
        }

        @Override
        public T next() {
            T datoG = aux.dato;
            aux = aux.sig;
            return datoG;
        }

    }

    public Iterator<T> getDescendingIterator() {
        return new IteradorDescendente();
    }

    public void insertarAlInicio(T nuevo) {
        NodoDoble<T> nuevoNodo = new NodoDoble<>(nuevo);
        nuevoNodo.sig = cabecera;
        if (cabecera != null) {
            cabecera.ant = nuevoNodo;
        }
        cabecera = nuevoNodo;
    }

    public void reiniciar() {
        cabecera = null;
    }

    @Override
    public String toString() {
        String cadena = "";
        NodoDoble<T> aux = cabecera;
        while (aux != null) {
            cadena = cadena + "\n" + aux.dato;
            aux = aux.sig;
        }
        return cadena;
    }

    public int numeroN() {
        NodoDoble<T> aux = cabecera;
        int c = 0;
        while (aux != null) {
            c++;
            aux = aux.sig;
        }
        return c;
    }

    public void eliminarPrimero() {
        if(cabecera.sig==null){
            cabecera=null;
        }else{
        cabecera = cabecera.sig;
        cabecera.ant = null;}
    }

    public void eliminarUltimo() {
        NodoDoble<T> aux = cabecera;
        while (aux.sig != null) {
            aux = aux.sig;
        }
        aux.ant.sig = null;
    }

    public void eliminarPosicion(int pos) {
        NodoDoble<T> aux = cabecera;
        int c = 1;
        while (c != pos) {
            aux = aux.sig;
            c++;
        }
        aux.ant.sig = aux.sig;
        aux.sig.ant = aux.ant;
    }

    public boolean consistenciarCodigo(T a) {
        NodoDoble<T> aux = cabecera;
        while (aux != null) {
            if (aux.dato.equals(a)) {
                return true;
            }
            aux = aux.sig;
        }
        return false;
    }
}
