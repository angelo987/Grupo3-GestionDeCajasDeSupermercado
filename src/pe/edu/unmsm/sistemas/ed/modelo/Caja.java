/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.sistemas.ed.modelo;


public class Caja {
    
    private int nCaja;
    private String codigo;
    private double total;
    private Usuario u;

    public Caja(int nCaja, String codigo, double total, Usuario user) {
        this.nCaja = nCaja;
        this.codigo = codigo;
        this.total = total;
        this.u = user;
    }

    public Caja(int num,Usuario u){
        this.u=u;
        nCaja=num;
        codigo="";
        total=0;
    }
    
    public Caja() {
        
    }

    public int getnCaja() {
        return nCaja;
    }

    public void setnCaja(int nCaja) {
        this.nCaja = nCaja;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Usuario getUser() {
        return u;
    }

    public void setUser(Usuario user) {
        this.u = user;
    }

    
    
    @Override
    public String toString() {
        return "Caja:" + "nCaja=" + nCaja + ", codigo=" + codigo +"total="+total+'}';
    }
    
    
}
