
package pe.edu.unmsm.sistemas.ed.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ConsultasUsuario extends Conexion{
    
    public boolean generarProducto(Producto pro) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConnection();
        String sql = "SELECT * FROM producto WHERE codigo = ?";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1,String.valueOf(rCodigoProducto()));
            rs = ps.executeQuery();          
            if(rs.next()){
                pro.setId(Integer.parseInt(rs.getString("id")));
                
                pro.setCodigo(rs.getString("codigo"));
                
                pro.setNombre(rs.getString("nombre"));
                
                pro.setPrecio(Double.parseDouble(rs.getString("precio")));
                
                pro.setCantidad(rStock());
                               
                return true;
            }
            return false;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }

    }
    
    public String cadena(){
        String todo = "";
        try {
           
            PreparedStatement ps = null;
            ResultSet rs = null;
            Connection con = getConnection(); 
            String sql = "SELECT numCaja,nombre FROM usuarios_caja";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            
            String aux="";
            while (rs.next()) {
                
                aux="Caja: "+rs.getInt("numCaja")+" \tUsuario: "+rs.getString("nombre");
                todo=todo+" "+aux+"\n";

            }

        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return todo;
    }
    
    public boolean eliminar(String cod){         
        PreparedStatement ps = null;
        Connection con = getConnection();
        
        String sql = "DELETE FROM registros WHERE codigo = ?";
        
        try{
            ps=con.prepareStatement(sql);
            ps.setString(1, cod);
            ps.execute();
            return true;
            
        }catch(SQLException e){
            System.err.println(e);
            return false;
        }finally{
            try{
                con.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }
    }    
    
    public boolean registrarVenta(Caja caj,ListaDoble<Producto> listaP){
        
        PreparedStatement ps = null;
        Connection con = getConnection();
        
        String sql = "INSERT INTO registros (nCaja,codigo,nombre,cantidad,precio,total) VALUES (?,?,?,?,?,?)";
        
        try{
            
            Iterator<Producto> iterador = listaP.getDescendingIterator(); 
        while(iterador.hasNext()){
            ps=con.prepareStatement(sql);
            Producto pro = iterador.next();
            ps.setInt(1, caj.getnCaja());
            ps.setString(2, caj.getCodigo());
            ps.setString(3, pro.getNombre());
            ps.setInt(4, pro.getCantidad());
            double tot = pro.getCantidad()*pro.getPrecio();
            ps.setDouble(5, tot);
            ps.setDouble(6,caj.getTotal());
            ps.execute();
        }

            
            return true;
            
        }catch(SQLException e){
            System.err.println(e);
            System.out.println("Holaaaaaaaaaaa");
            return false;
        }finally{
            try{
                con.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }
    }
    
    public boolean registrar(Usuario usr){
        
        PreparedStatement ps=null;
        
        
        //String sql="INSERT INTO usuarios_caja(usuario,password,nombre) VALUES(?,?,?)";
        
        
        try {
            Connection con=getConnection();
            ps=con.prepareStatement("INSERT INTO usuarios_caja (numCaja,usuario,nombre,password) VALUES(?,?,?,?)");      
            ps.setInt(1,usr.getnCaja());
            ps.setString(2,usr.getUsser());
            ps.setString(3,usr.getNombre());
            ps.setString(4,usr.getPassword());
            ps.execute();
            return true;           
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error con la base de datos.");
            return false;
        }        
    }
    
    public boolean login(Usuario user){
        
        PreparedStatement ps=null;
        ResultSet rs=null;
        Connection con=getConnection();
        
        String sql="SELECT * FROM usuarios_caja WHERE usuario = ?";
        try {
            ps=con.prepareStatement(sql);
            ps.setString(1,user.getUsser());
            rs=ps.executeQuery();
            
            if(rs.next()){
                if(user.getPassword().equals(rs.getString("password"))==true){                 
                    user.setId(rs.getInt("id"));
                    user.setNombre(rs.getString("nombre"));
                    user.setnCaja(rs.getInt("numCaja"));
                    return true; 
                }else{
                    return false;
                }  
            }
            
            return false;
    
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se encontró en base de datos.");
            return false;
        }
         
    }
    
    public int rStockLista(){
        double r = Math.random();
        return (int) ((r*9)+1);
    }
    
    public int rCodigoProducto() {     
        
        return (int)(((Math.random()*((100+cantidadProductos())-101)))+101);
    }
    
    public int rStock(){
        double r = Math.random();
        return (int) ((r*12)+1);
    }
    
    public int cantidadProductos(){
        int c=0;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConnection();
        String sql = "SELECT codigo,nombre,precio FROM producto" ;
        try{    
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                c++;
            }
            return c;
        }catch(SQLException e){
            System.err.println(e);
            return 0;
        }finally{
            try{
                con.close();
            }catch(SQLException e){
                System.err.println(e);
            }
        }
    }
}
