/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.unmsm.sistemas.ed.modelo;

import java.util.Iterator;

public class Pila<T> {

    Nodo<T> cima;

    public Nodo<T> getCima() {
        return cima;
    }

    public void setCima(Nodo<T> cima) {
        this.cima = cima;
    }

    public void apilar(T elementoNuevo) {
        Nodo<T> nuevo = new Nodo<>(elementoNuevo);
        nuevo.sgte = cima;
        cima = nuevo;
    }
    
    public void reiniciar(){
        cima=null;
    }
    
    public boolean comparar(Producto a){
        Nodo<Producto> aux = (Nodo<Producto>) cima;
        while(aux!=null){
            if(aux.dato.getCodigo().equals(a.getCodigo())){
                return true;
            }
            aux=aux.sgte;
        }
        return false;
    }
    
    public T desapilar() {
        T aux = cima.dato;
        cima = cima.sgte;
        return aux;
    }

    public int cantidad(){
        Nodo<T> aux = cima;
        int c=0;
        while (aux != null) {
            c++;
            aux = aux.sgte;
        }
        return c;
        }
    
    public String mostrarElementos() {
        Nodo<T> aux = cima;
        String represenCad = "";
        while (aux != null) {
            represenCad = aux.dato + " - " +represenCad;
            aux = aux.sgte;
        }
        return represenCad;
    }

    @Override
    public String toString() {
        return "Pila{" + "cima=" + cima + '}';
    }
    
    
    class IteradorDescendente implements Iterator<T> {

        Nodo<T> aux;
        
        public IteradorDescendente(){
           aux=cima;
        }
        
        @Override
        public boolean hasNext() {
            return aux != null;
        }

        @Override
        public T next() {
            T dato = aux.dato;
            aux = aux.sgte;
            return dato;
        }

    }

    public Iterator<T> getDescendingIterator() {
      return new IteradorDescendente();
    }
    
    
    

}
