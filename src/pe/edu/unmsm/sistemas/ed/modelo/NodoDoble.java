
package pe.edu.unmsm.sistemas.ed.modelo;


public class NodoDoble<T> {
    
    NodoDoble<T> sig;
    NodoDoble<T> ant;
    T dato;

    public NodoDoble(T dato) {
        this.dato = dato;
    }

}
