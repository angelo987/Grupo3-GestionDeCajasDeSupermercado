
package pe.edu.unmsm.sistemas.ed.controlador;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import pe.edu.unmsm.sistemas.ed.modelo.Caja;
import pe.edu.unmsm.sistemas.ed.modelo.ConsultasUsuario;
import pe.edu.unmsm.sistemas.ed.modelo.ListaDoble;
import pe.edu.unmsm.sistemas.ed.modelo.Pila;
import pe.edu.unmsm.sistemas.ed.modelo.Producto;
import pe.edu.unmsm.sistemas.ed.modelo.Usuario;
import pe.edu.unmsm.sistemas.ed.modelo.hash;
import pe.edu.unmsm.sistemas.ed.vista.frmAdmCajas;
import pe.edu.unmsm.sistemas.ed.vista.frmAdmProductos;
import pe.edu.unmsm.sistemas.ed.vista.frmCaja;
import pe.edu.unmsm.sistemas.ed.vista.frmJefe;
import pe.edu.unmsm.sistemas.ed.vista.frmLoginJ;
import pe.edu.unmsm.sistemas.ed.vista.frmLogin;
import pe.edu.unmsm.sistemas.ed.vista.frmPrincipal;
import pe.edu.unmsm.sistemas.ed.vista.frmRegistro;



public class CtrlUsuario implements ActionListener{
    
    private Producto modP;
    private ConsultasUsuario modC;
    private frmCaja frm;
    private Pila<Producto> modPila;
    private ListaDoble<Producto> modLista;
    private ListaDoble<String> modListaC;
    private Caja caja;
    private frmPrincipal frmPrin;
    private frmLogin frmLo;
    private Usuario user;
    private frmJefe frmJ;
    private frmAdmCajas frmAC;
    private frmAdmProductos frmAPro;
    private frmRegistro frmR;
    private frmLoginJ frmLoJ;
    private ListaDoble<Usuario> modLU;
    
    private static final String USER = "admin123";
    private static final String PASSWORD = "qwerty";
    
    
    public CtrlUsuario(Producto modP, ConsultasUsuario modC, frmCaja frm, Pila<Producto> modPila, ListaDoble<Producto> modLista,ListaDoble<String> modListaC,Caja caja,
            frmPrincipal frmPrim, frmLogin frmLo,Usuario user,frmJefe frmJ,frmAdmCajas frmAC,frmAdmProductos frmAPro
            ,frmRegistro frmR,frmLoginJ frmLoJ,ListaDoble<Usuario> modLU) {
        this.modP = modP;
        this.modC = modC;
        this.frm = frm;
        this.modPila= modPila;
        this.modLista=modLista;
        this.modListaC=modListaC;
        this.caja=caja;
        this.frmPrin=frmPrim;
        this.frmLo=frmLo;
        this.user=user;
        this.frmJ=frmJ;
        this.modLU=modLU;
        this.frmAC=frmAC;
        this.frmAPro=frmAPro;
        this.frmR=frmR;
        this.frmLoJ=frmLoJ;
        this.frm.btnGenerarC.addActionListener(this);
        this.frm.btnEscanear.addActionListener(this);
        this.frm.btnReiniciar.addActionListener(this);
        this.frm.btnCobrar.addActionListener(this);
        this.frm.btnConfirmar.addActionListener(this);
        this.frm.btnGenerarB.addActionListener(this);
        this.frm.btnEliminar.addActionListener(this);
        this.frmPrin.btnCajero.addActionListener(this);
        this.frmLo.btnIngreso.addActionListener(this);
        this.frm.btnCerrar.addActionListener(this);
        this.frmPrin.btnJefe.addActionListener(this);
        this.frmAC.btnAgregarCaja.addActionListener(this);
        this.frmJ.btnAdmC.addActionListener(this);
        this.frmJ.btnAdmP.addActionListener(this);
        this.frmR.btnRegistrar.addActionListener(this);
        this.frmLoJ.btnIng.addActionListener(this);
        this.frmAC.btnRegresarAC.addActionListener(this);
        this.frmR.btnRR.addActionListener(this);
        this.frmJ.btnCerrars.addActionListener(this);
        this.frmAC.btnMostrar.addActionListener(this);
        //this.frmAC.btnEliminarCaja.addActionListener(this);
    }
    
    public void iniciar(){
        
        frm.setTitle("MODO CAJA");
        frmJ.setTitle("MODO JEFE");
        frmPrin.setTitle("MENU PRINCIPAL");
        frmLo.setTitle("LOGIN USUARIO");
        frmLoJ.setTitle("LOGIN JEFE");
        frmAC.setTitle("ADMINISTRACION DE CAJA");
        frmAPro.setTitle("GESTION DE PRODUCTO");
        frmR.setTitle("REGISTRO");
        frm.setLocationRelativeTo(null);
        frmJ.setLocationRelativeTo(null);
        frmPrin.setLocationRelativeTo(null);
        frmLo.setLocationRelativeTo(null);
        frmLoJ.setLocationRelativeTo(null);
        frmAC.setLocationRelativeTo(null);
        frmAPro.setLocationRelativeTo(null);
        frmR.setLocationRelativeTo(null);
        
        
        frm.txtTotal.setText(String.valueOf(caja.getTotal()));
        frm.txtCodigoV.setEditable(false);
        frm.txtTotal.setEditable(false);
        frm.btnCobrar.setEnabled(false);
        frm.btnGenerarB.setEnabled(false);
        frm.btnConfirmar.setEnabled(false);
        frm.txtnCaja.setEditable(false);
        frm.txtnCaja.setText(String.valueOf(user.getnCaja()));
        frm.setResizable(false);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == frm.btnGenerarC){
            modPila.reiniciar();
            modListaC.reiniciar();
            for(int i=1;i<=modC.rStockLista();i++){  
                if(modC.generarProducto(modP)){               
                Producto paux = new Producto();
                paux.setCantidad(modP.getCantidad());
                paux.setCodigo(modP.getCodigo());
                paux.setId(modP.getId());
                paux.setNombre(modP.getNombre());
                paux.setPrecio(modP.getPrecio()); 
                if(modListaC.consistenciarCodigo(paux.getCodigo())==false){
                    modPila.apilar(paux);
                }
                modListaC.insertarAlInicio(paux.getCodigo());
            } 
        }
            JOptionPane.showMessageDialog(null,"Cliente Generado.");
            mostrarProductosCliente();           
        }
        if(e.getSource() == frm.btnEscanear){
                frm.btnGenerarC.setEnabled(false);
                if(modPila.cantidad()==0){
                    JOptionPane.showMessageDialog(null, "No hay productos que escanear.");
                }else{
                Producto aux=modPila.desapilar();                
                double ant =aux.getCantidad()*aux.getPrecio();
                caja.setTotal(caja.getTotal()+ant);
                frm.txtTotal.setText(String.valueOf(caja.getTotal()));
                modLista.insertarAlInicio(aux);
                mostrarProductosCliente();
                mostrarProductosTrabajador();   
                frm.btnConfirmar.setEnabled(true);
            }
        }
        if(e.getSource() == frm.btnReiniciar){
            frm.btnGenerarC.setEnabled(true);
            modLista.reiniciar();
            caja.setTotal(0.0);
            frm.txtTotal.setText(String.valueOf(caja.getTotal()));
            mostrarProductosTrabajador();
            JOptionPane.showMessageDialog(null, "Consulta limpiada.");
        }
        if(e.getSource() == frm.btnCobrar){            
            caja.setnCaja(Integer.parseInt(frm.txtnCaja.getText()));
            if(modC.registrarVenta(caja, modLista)){
                JOptionPane.showMessageDialog( null , "Venta realizada satisfactoriamente.");
            }else{
               JOptionPane.showMessageDialog( null , "Error al guardar venta.");
            } 
            frm.btnCobrar.setEnabled(false);
            frm.btnGenerarB.setEnabled(true);   
        }
        if(e.getSource() == frm.btnConfirmar){
            caja.setCodigo(generarCodigoVenta());
            frm.txtCodigoV.setText(caja.getCodigo());
            //caja.getListaCodigos().insertarAlInicio(caja.getCodigo());
            frm.btnEscanear.setEnabled(false);
            frm.btnEliminar.setEnabled(false);
            frm.btnReiniciar.setEnabled(false);
            frm.btnCobrar.setEnabled(true);
            frm.btnConfirmar.setEnabled(false);
        }
        if(e.getSource() == frm.btnGenerarB){
            try{
                FileOutputStream archivo;
                File file = new File("C:\\Users\\USER\\Desktop\\Proyecto8.0\\src\\comprobantes\\"+caja.getCodigo()+".pdf");
                archivo = new FileOutputStream(file);
                Document doc = new Document();
                PdfWriter.getInstance(doc, archivo);
                doc.open();
                //Logo
                Image img = Image.getInstance("C:\\Users\\USER\\Desktop\\Proyecto8.0\\src\\img\\Logo.png");              
                img.setAlignment(Element.ALIGN_LEFT);
                img.scaleToFit(120, 120);
                doc.add(img);
                //Titulo
                Paragraph p = new  Paragraph(10);
                Font fuente = new Font(Font.FontFamily.TIMES_ROMAN,15,Font.BOLD,BaseColor.BLACK);
                p.add(Chunk.NEWLINE);
                p.add(Chunk.NEWLINE);
                p.add(Chunk.NEWLINE);              
                p.add("LA CASA DEL AHORRO");
                p.add(Chunk.NEWLINE);
                p.add(Chunk.NEWLINE);
                p.add("BOLETA DE VENTA ELECTRONICA");
                p.add(Chunk.NEWLINE);
                p.add(Chunk.NEWLINE);
                p.add("CODIGO DE VENTA:   "+caja.getCodigo());
                p.add(Chunk.NEWLINE);
                p.add(Chunk.NEWLINE);
                p.setAlignment(Element.ALIGN_CENTER);
                doc.add(p);
                //Tabla
                PdfPTable tabla = new PdfPTable(5);
                tabla.setWidthPercentage(100);
                PdfPCell c1 = new PdfPCell(new Phrase("Código",fuente));
                PdfPCell c2 = new PdfPCell(new Phrase("Producto",fuente));
                PdfPCell c3 = new PdfPCell(new Phrase("Unidades",fuente));
                PdfPCell c4 = new PdfPCell(new Phrase("Precio",fuente));
                PdfPCell c5 = new PdfPCell(new Phrase("Total",fuente));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c2.setHorizontalAlignment(Element.ALIGN_CENTER);
                c3.setHorizontalAlignment(Element.ALIGN_CENTER);
                c4.setHorizontalAlignment(Element.ALIGN_CENTER);
                c5.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBackgroundColor(BaseColor.GRAY);
                c2.setBackgroundColor(BaseColor.GRAY);
                c3.setBackgroundColor(BaseColor.GRAY);
                c4.setBackgroundColor(BaseColor.GRAY);
                c5.setBackgroundColor(BaseColor.GRAY);
                tabla.addCell(c1);
                tabla.addCell(c2);
                tabla.addCell(c3);
                tabla.addCell(c4);
                tabla.addCell(c5);
                
                Iterator<Producto> iterador = modLista.getDescendingIterator(); 
                while(iterador.hasNext()){
                Producto pro = iterador.next();                                      
                    tabla.addCell(pro.getCodigo());
                    tabla.addCell(pro.getNombre());
                    tabla.addCell(String.valueOf(pro.getCantidad()));
                    tabla.addCell(String.valueOf(pro.getPrecio()));
                    tabla.addCell(String.valueOf(pro.getCantidad()*pro.getPrecio()));                           
                }
                doc.add(tabla);
                
                Paragraph p1 = new  Paragraph(10);
                p1.add(Chunk.NEWLINE);
                p1.add("APORTE TOTAL:"+frm.txtTotal.getText());
                p1.add(Chunk.NEWLINE);               
                p1.setAlignment(Element.ALIGN_RIGHT);
                p1.add(Chunk.NEWLINE);
                p1.add(Chunk.NEWLINE);
                doc.add(p1);
                
                Paragraph p2 = new  Paragraph(10);
                p2.add(Chunk.NEWLINE);
                p2.add(Chunk.NEWLINE);
                p2.add("Estimado cliente, conserve esta boleta para futuros reclamos o devoluciones.");
                p2.add(Chunk.NEWLINE);
                p2.add(Chunk.NEWLINE);
                p2.add("Gracias por comprar en: LA CASA DEL AHORRO");
                p2.add(Chunk.NEWLINE);
                p2.add(Chunk.NEWLINE);
                
                Calendar calendario = Calendar.getInstance();
                
                p2.add("Fecha de Emision: "+calendario.get(Calendar.DAY_OF_MONTH)+"/"+"0"+(calendario.get(Calendar.MONTH)+1)+"/"+(calendario.get(Calendar.YEAR)-2000)+"   "+"Hora: "+calendario.get(Calendar.HOUR_OF_DAY)+":"+calendario.get(Calendar.MINUTE)+":"+calendario.get(Calendar.SECOND));
                p2.setAlignment(Element.ALIGN_CENTER);
                p2.add(Chunk.NEWLINE);
                p2.add(Chunk.NEWLINE);
                p2.add("Usted fue atendido por "+user.getNombre()+" - Caja: "+user.getnCaja());
                p2.add(Chunk.NEWLINE);
                p2.add(Chunk.NEWLINE);
                doc.add(p2);
                
                doc.close();
                archivo.close();
                Desktop.getDesktop().open(file);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DocumentException ex) {
                Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            frm.btnEscanear.setEnabled(true);
            frm.btnReiniciar.setEnabled(true);
            frm.btnGenerarC.setEnabled(true);
            frm.txtCodigoV.setText("");
            frm.txtTotal.setText("");
            caja.setTotal(0.0);
            frm.btnGenerarB.setEnabled(false);
            frm.btnEliminar.setEnabled(true);
            modLista.reiniciar();
            mostrarProductosTrabajador();
        }
        if(e.getSource() == frm.btnEliminar){
            int num = modLista.numeroN();
            Iterator<Producto> iterador = modLista.getDescendingIterator();
            int c = 0;
            int t = 0;
            int con=0;
            while (iterador.hasNext()) {
                Producto pro = iterador.next();
                double cant = pro.getCantidad()*pro.getPrecio();
                c++;
                if (pro.getCodigo().equals(frm.txtCodP.getText())) {
                    if(num != 1){    
                    if (c == 1) {   
                        caja.setTotal(caja.getTotal()-cant);
                        frm.txtTotal.setText(String.valueOf(caja.getTotal()));
                        modLista.eliminarPrimero();              
                    }
                    if (c == num) {
                        caja.setTotal(caja.getTotal()-cant);
                        frm.txtTotal.setText(String.valueOf(caja.getTotal()));
                        modLista.eliminarUltimo();      
                    }
                    if (c != 1 && c != num) {                       
                        caja.setTotal(caja.getTotal()-cant);
                        frm.txtTotal.setText(String.valueOf(caja.getTotal()));
                        modLista.eliminarPosicion(c);    
                    }
                    }else{
                        caja.setTotal(caja.getTotal()-cant);
                        frm.txtTotal.setText(String.valueOf(caja.getTotal()));
                        modLista.eliminarPrimero();
                    }
                }else{
                  t++;  
                }
            }
            if (t == num) {
                JOptionPane.showMessageDialog(null, "Código erroneo, vuelva a intentarlo.");
            }
            if(modLista.numeroN()==0){
                frm.btnGenerarC.setEnabled(true);
            }
            frm.txtCodP.setText("");
            mostrarProductosTrabajador();
        }
        
        if(e.getSource() == frmPrin.btnCajero){
            frmLo.setVisible(true);         
        }  
        if (e.getSource() == frmPrin.btnJefe) {
            frmLoJ.setVisible(true);   
        }
        if (e.getSource() == frmLoJ.btnIng) {
                if (frmLoJ.txtUSER.getText().equals(USER) && frmLoJ.txtPASSWORD.getText().equals(PASSWORD)) {
                    JOptionPane.showMessageDialog(null, "Verificacion realizada.");
                    frmLoJ.txtUSER.setText("");
                    frmLoJ.txtPASSWORD.setText("");
                    frmLoJ.setVisible(false);
                    frmJ.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "Contraseña incorrecta, vuelva a intentar.");
                    frmLoJ.txtUSER.setText("");
                    frmLoJ.txtPASSWORD.setText("");
                }
            }
        
        if (e.getSource() == frmLo.btnIngreso) {
            String pass = new String(frmLo.txtPassword.getPassword());
            if (frmLo.txtUsuario.getText().equals("") == false) {
                String nuevoPass = hash.sha1(pass);
                user.setUsser(frmLo.txtUsuario.getText());
                user.setPassword(nuevoPass);

                if (modC.login(user)) {
                    frm.setVisible(true);
                    frmLo.setVisible(false);
                    frm.txtnCaja.setText(String.valueOf(user.getnCaja()));
                    frmLo.txtPassword.setText("");
                    frmLo.txtUsuario.setText("");
                    Usuario aux = user;
                    modLU.insertarAlInicio(aux);                
                } else {
                    JOptionPane.showMessageDialog(null, "DATOS INCORRECTOS.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "INGRESE DATOS.");
            }
        }
        if(e.getSource() == frm.btnCerrar){
            frm.setVisible(false);          
        }
        if(e.getSource() == frmJ.btnAdmC){
            frmAC.setVisible(true);
        }
        if(e.getSource() == frmAC.btnAgregarCaja){
            frmR.setVisible(true);   
        }
        //if(e.getSource() == frmAPro)
        if (e.getSource() == frmR.btnRegistrar) {
            String pass = new String(frmR.txtPass.getPassword());
            String passCon = new String(frmR.txtConfirmPass.getPassword());
            
            if (frmR.txtUser.getText().equals("") || pass.equals("") || passCon.equals("")
                    || frmR.txtNombre.getText().equals("") || frmR.txtNumCaja.getText().equals("")) {
                //JOptionPane.showMessageDialog(null, "Hay campos vacios debe llenar todos"
                //      + " los campos");
            } else {
                
                if (pass.equals(passCon)) {
                    String nuevoPass = hash.sha1(pass);
                    
                    user.setUsser(frmR.txtUser.getText());
                    user.setPassword(nuevoPass);
                    user.setNombre(frmR.txtNombre.getText());
                    user.setnCaja(Integer.parseInt(frmR.txtNumCaja.getText()));
                    if (modC.registrar(user)) {
                        JOptionPane.showMessageDialog(null, "Registro Correcto. ");
                        limpiarRegistro();
                        frmR.setVisible(false);
                    } else {
                        JOptionPane.showMessageDialog(null, "Error al guardar.");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Las contraseñas no coinciden. ");
                }
            }            
        }
        
        if(e.getSource() == frmJ.btnAdmP){
            frmAPro.setVisible(true);
        }
        if(e.getSource() == frmAC.btnRegresarAC){
            frmAC.setVisible(false);
        }
        if(e.getSource() == frmR.btnRR){
            frmR.setVisible(false);
        }
        if(e.getSource() == frmJ.btnCerrars){
            frmJ.setVisible(false);
        }
        if(e.getSource() == frmAC.btnMostrar){
            frmAC.txtResul.setText(modC.cadena());
        }
        //if(e.getSource() == frmAC.btnEliminarCaja){
          //  boolean flag = true;
            //while(flag==true){
              //  flag=modC.eliminar(frmAC.txtNumAdCaja.getText());
            //}
            //JOptionPane.showMessageDialog(null, "Venta eliminada del sistema.");
        //}
  
    }
        
    public void mostrarProductosCliente(){
        DefaultTableModel modelo = (DefaultTableModel) frm.tblCliente.getModel();
        modelo.setRowCount(0);
        Iterator<Producto> iterador = modPila.getDescendingIterator(); 
        while(iterador.hasNext()){
            Producto pro = iterador.next();
            modelo.addRow(new Object[]{pro.getCodigo(), pro.getNombre(), pro.getCantidad()});          
        }    
    }
    
    private void limpiarRegistro() {
        frmR.txtConfirmPass.setText("");
        frmR.txtNombre.setText("");
        frmR.txtPass.setText("");
        frmR.txtUser.setText("");
        frmR.txtNumCaja.setText("");

    }
    
    public void mostrarProductosTrabajador(){
        DefaultTableModel modelo = (DefaultTableModel) frm.tblCaja.getModel();
        modelo.setRowCount(0);
        Iterator<Producto> iterador = modLista.getDescendingIterator(); 
        while(iterador.hasNext()){
            Producto pro = iterador.next();
            modelo.addRow(new Object[]{pro.getCodigo(), pro.getNombre(), pro.getCantidad(), (pro.getCantidad()*pro.getPrecio())});          
        }    
    }

    public String generarCodigoVenta() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-", "").substring(25, uuid.replace("-", "").length());
    }

    public Caja getCaja() {
        return caja;
    }

    public void setCaja(Caja caja) {
        this.caja = caja;
    }
  
}

