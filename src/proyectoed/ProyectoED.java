
package proyectoed;

import pe.edu.unmsm.sistemas.ed.controlador.CtrlUsuario;
import pe.edu.unmsm.sistemas.ed.modelo.Caja;
import pe.edu.unmsm.sistemas.ed.modelo.ConsultasUsuario;
import pe.edu.unmsm.sistemas.ed.modelo.ListaDoble;
import pe.edu.unmsm.sistemas.ed.modelo.Pila;
import pe.edu.unmsm.sistemas.ed.modelo.Producto;
import pe.edu.unmsm.sistemas.ed.modelo.Usuario;
import pe.edu.unmsm.sistemas.ed.vista.frmAdmCajas;
import pe.edu.unmsm.sistemas.ed.vista.frmAdmProductos;
import pe.edu.unmsm.sistemas.ed.vista.frmCaja;
import pe.edu.unmsm.sistemas.ed.vista.frmJefe;
import pe.edu.unmsm.sistemas.ed.vista.frmLogin;
import pe.edu.unmsm.sistemas.ed.vista.frmLoginJ;
import pe.edu.unmsm.sistemas.ed.vista.frmPrincipal;
import pe.edu.unmsm.sistemas.ed.vista.frmRegistro;

public class ProyectoED {

    public static void main(String[] args) { 
        Producto pr = new Producto();
        frmCaja frmCa = new frmCaja();
        ConsultasUsuario cpro = new ConsultasUsuario();
        Pila<Producto> pila = new Pila();
        ListaDoble<Producto> lista = new ListaDoble();
        ListaDoble<String> listaC = new ListaDoble();
        Caja caja = new Caja();
        frmPrincipal frmP = new frmPrincipal();
        frmLogin frmLo = new frmLogin();
        Usuario user = new Usuario();
        frmJefe frmJ = new frmJefe();
        frmAdmCajas frmAC = new frmAdmCajas();
        frmAdmProductos frmAPro = new frmAdmProductos();
        frmRegistro frmR = new frmRegistro();
        frmLoginJ frmLoJ = new frmLoginJ();
        ListaDoble<Usuario> usu = new ListaDoble();
        CtrlUsuario ctrl = new CtrlUsuario(pr,cpro,frmCa,pila,lista,listaC,caja,frmP,frmLo,user,frmJ,frmAC,frmAPro,frmR,frmLoJ,usu);
        ctrl.iniciar();
        frmP.setVisible(true);
        //frmCa.setVisible(true);
          
    }
}
